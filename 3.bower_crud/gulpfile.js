var gulp = require('gulp');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var ngmin = require('gulp-ngmin');

gulp.task('lint', function() {
    gulp.src('./app/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('minify', function(){
    gulp.src('./app/*.js')
        .pipe(concat('all.js'))
        .pipe(ngmin())
        .pipe(gulp.dest('./dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist'));
});

gulp.task('default', function(){
    gulp.run('lint', 'minify');

    gulp.watch("./app/*.js", function(event){
        gulp.run('lint', 'minify');
    });
});