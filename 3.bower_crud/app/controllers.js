usersApp.controller('usersController', function ($scope, $http) {

    $http.get('/users').
        success(function(data, status, headers, config) {
            $scope.users = data;
        });

    $scope.addUser = function() {
        // show modal with form for user add
        var $scope = $("#userForm").scope();
        $scope.user = {firstname:'', lastname:'', id:''};
        $('#editModal').modal('show');
    };
});

usersApp.controller('saveUserController', function ($scope, $http) {

    $scope.user = {firstname:'', lastname:'', id:''};

    $scope.submit = function() {

        if ($scope.user.id.length === 0) {
            $http.post('/users', $scope.user).
                success(function(data, status, headers, config) {
                    window.location.reload();
                });
        } else {
            $http.put('/users', $scope.user).
                success(function(data, status, headers, config) {
                    window.location.reload();
                });
        }
    };
});

usersApp.controller('removeUserController', function ($scope, $http) {

    $scope.removeUser = function(user) {
        $http.delete('/users/' + user.id).
            success(function(data, status, headers, config) {
                window.location.reload();
            });
    };
});


usersApp.controller('editUserController', function ($scope, $http) {

    $scope.getUser = function(user) {
        $http.get('/users/' + user.id).
            success(function(data, status, headers, config) {

                var $scope = $("#userForm").scope();
                $scope.user = data;

                $('#editModal').modal('show');
            });
    };
});
