var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var users = require('./routes/users');
var comments = require('./routes/comments');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use('/app',  express.static(__dirname + '/app'));
app.use('/dist',  express.static(__dirname + '/dist'));

app.use('/', users);
app.use('/', comments);

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            error: err.message
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send('error', {
        message: err.message,
        error: {}
    });
});

app.get('*', function(req, res) {
    res.sendFile('/public/index.html');
});

module.exports = app;
