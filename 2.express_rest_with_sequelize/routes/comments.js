var models  = require('../models');
var express = require('express');
var router  = express.Router();

/**
 * Get all comments
 */
router.get('/comments', function(req, res) {
    models.Comment.findAll().then(function(comments) {
        res.send(comments);
    });
});

/**
 * Get user's comments
 */
router.get('/users/:user_id/comments', function(req, res) {
    models.Comment.findAll({
        where: {userId: req.param('user_id')}
    }).then(function(comments) {
        res.send(comments);
    });
});

/**
 * Get user's comment by id
 */
router.get('/users/:user_id/comments/:comment_id', function(req, res) {
    models.Comment.find({
        where: {userId: req.param('user_id'), id: req.param('comment_id')}
    }).then(function(comment) {
        res.send(comment);
    });
});

/**
 * Remove user's comment
 */
router.delete('/users/:user_id/comments/:comment_id', function(req, res) {
    models.Comment.find({
        where: {id: req.param('comment_id'), userId: req.param('user_id')}
    }).then(function(comment) {
        comment.destroy().then(function() {
            res.send({'status': 'success'});
        });
    });
});

/**
 * Add comment
 */
router.post('/users/:user_id/comments', function(req, res) {
    models.User.find({
        where: { id: req.param('user_id') }
    }).then(function(user) {
        models.Comment.create({
            text: req.param('text')
        }).then(function(text) {
            text.setUser(user).then(function() {
                res.send({'status': 'success'});
            });
        });
    });
});

/**
 * Update comment
 */
router.put('/users/:user_id/comments/:comment_id', function(req, res) {
    models.Comment.find({
        where: {id: req.param('comment_id'), userId: req.param('user_id')}
    }).then(function(comment) {
        comment.update({
            text: req.param('text')
        }).then(function() {
            res.send({'status': 'success'});
        });
    });
});

module.exports = router;