var models  = require('../models');
var express = require('express');
var router  = express.Router();

/**
 * get all users (index page)
 */
router.get('/', function(req, res) {
    res.redirect('/users');
});

/**
 * get all users
 */
router.get('/users', function(req, res) {
    models.User.findAll().then(function(users) {
        res.send(users);
    });
});

/**
 * get user
 */
router.get('/users/:id', function(req, res) {
    models.User.find({
        where: {id: req.param('id')}
    }).then(function(user) {
        res.send(user);
    });
});

/**
 * create user
 */
router.post('/users', function(req, res) {
    models.User.create({
        firstname: req.param('firstname'),
        lastname: req.param('lastname')
    }).then(function() {
        res.send({'status': 'success'});
    });
});

/**
 * remove user
 */
router.delete('/users/:id', function(req, res) {
    models.User.find({
        where: {id: req.param('id')}
    }).then(function(user) {
        user.destroy().then(function() {
            res.send({'status': 'success'});
        });
    });
});

/**
 * update user
 */
router.put('/users/:id', function(req, res) {
    models.User.find({
        where: {id: req.param('id')}
    }).then(function(user) {
        user.update({
            firstname: req.param('firstname'),
            lastname: req.param('lastname')
        }).then(function() {
            res.send({'status': 'success'});
        });
    });
});

module.exports = router;