module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define("user", {
        firstname: DataTypes.STRING,
        lastname: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                User.hasMany(models.Comment)
            }
        }
    });

    return User;
};