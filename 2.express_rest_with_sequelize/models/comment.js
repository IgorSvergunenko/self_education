module.exports = function(sequelize, DataTypes) {
    var Comment = sequelize.define("comment", {
        text: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                Comment.belongsTo(models.User);
            }
        }
    });

    return Comment;
};