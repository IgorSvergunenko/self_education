var pg = require('pg');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser());

var dbUrl = "pg://postgres:postgres@localhost:5432/nodejs_crud";

app.get('/', function(req, res) {
    res.send('Hello world');
});

app.get('/users', function(req, res) {
    pg.connect(dbUrl, function(err, client) {
        client.query("SELECT id, firstname, lastname FROM users ORDER BY id ASC", function(err, result) {
            if (err) {
                return res.send(err.name);
            } else {
                res.send(result.rows);
            }
        });
    });
});

app.get('/users/:id', function(req, res) {
    pg.connect(dbUrl, function(err, client) {
        client.query("SELECT id, firstname, lastname FROM users WHERE id=$1", [req.params.id], function(err, result) {
            if (err) {
                return res.send({'result': 'error'});
            } else {
                res.send(result.rows);
            }
        });
    });
});

app.post('/users', function(req, res) {
    pg.connect(dbUrl, function(err, client) {
        client.query("INSERT INTO users(firstname, lastname) values($1, $2)", [req.body.firstname, req.body.lastname], function(err, result) {
            if (err) {
                return res.send({'result': 'error'});
            } else {
                res.send({'result': 'success'});
            }
        });
    });
});

app.put('/users/:id', function(req, res) {
    pg.connect(dbUrl, function(err, client) {
        client.query("UPDATE users set firstname = $1, lastname = $2 WHERE id = $3", [req.query.firstname, req.query.lastname, req.params.id], function(err, result) {
            if (err) {
                return res.send({'result': 'error'});
            } else {
                res.send({'result': 'success'});
            }
        });
    });
});

app.delete('/users/:id', function(req, res) {
    pg.connect(dbUrl, function(err, client) {
        client.query("DELETE FROM users  WHERE id = $1", [req.params.id], function(err, result) {
            if (err) {
                return res.send({'result': 'error'});
            } else {
                res.send({'result': 'success'});
            }
        });
    });
});

app.listen(8888)