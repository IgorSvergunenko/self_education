var server = require("./start");
var requestHandlers = require("./requestHandlers");

var handle = {}
handle["/"] = requestHandlers.users;
handle["/users"] = requestHandlers.users;
handle["/create"] = requestHandlers.create;
handle["/remove"] = requestHandlers.remove;
handle["/update"] = requestHandlers.update;
handle["/edit"] = requestHandlers.edit;

server.start(handle);