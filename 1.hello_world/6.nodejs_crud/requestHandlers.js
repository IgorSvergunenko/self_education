var ejs = require('ejs'),
    fs = require("fs"),
    url = require("url"),
    pg = require("pg"),
    qs = require('querystring'),
    conString = "pg://postgres:postgres@localhost:5432/nodejs_crud";

// connect to db
var client = new pg.Client(conString);
client.connect();

//client.query("CREATE TABLE IF NOT EXISTS users(id SERIAL, firstname varchar(64), lastname varchar(64))");
//client.query("INSERT INTO users(firstname, lastname) values($1, $2)", ['Chuck', 'Berry']);
//client.query("INSERT INTO users(firstname, lastname) values($1, $2)", ['BB', 'King']);
//client.query("INSERT INTO users(firstname, lastname) values($1, $2)", ['Buddy', 'Guy']);
//client.query("INSERT INTO users(firstname, lastname) values($1, $2)", ['Eric', 'Clapton']);

/**
 * Show list of users
 * @param request
 * @param response
 */
function users(request, response) {

    var query = client.query("SELECT id, firstname, lastname FROM users");

    query.on("row", function (row, result) {
        result.addRow(row);
    });

    query.on("end", function (result) {

        var result = result.rows;
        var content = fs.readFileSync("users.html", "utf8");
        var renderedHtml = ejs.render(content, {users: result});

        response.writeHead(200, {"Content-Type": "text/html", "Location": "/"});
        response.write(renderedHtml);
        response.end();
    });
}

/**
 * Add user
 * @param request
 * @param response
 */
function create(request, response) {

    var body = '';
    request.on('data', function (data) {
        body += data;
    });

    request.on('end', function () {
        var post = qs.parse(body);
        client.query("INSERT INTO users(firstname, lastname) values($1, $2)", [post.firstname, post.lastname]);
        response.writeHead(302, {'Location': '/'});
        response.end();
    });
}


/**
 * Page with selected user data for edit
 *
 * @param request
 * @param response
 */
function edit(request, response) {

    params = url.parse(request.url, true).query;
    var query = client.query("SELECT id, firstname, lastname FROM users WHERE id = $1", [params.id]);

    query.on("row", function (row, result) {
        result.addRow(row);
    });

    query.on("end", function (result) {

        var result = result.rows;
        var content = fs.readFileSync("edit.html", "utf8");
        var renderedHtml = ejs.render(content, {users: result});

        response.writeHead(200, {"Content-Type": "text/html"});
        response.write(renderedHtml);
        response.end();
    });
}

/**
 * @param request
 * @param response
 */
function update(request, response) {

    var body = '';
    request.on('data', function (data) {
        body += data;
    });

    request.on('end', function () {
        var post = qs.parse(body);
        client.query("UPDATE users set firstname = $1, lastname = $2 WHERE id = $3", [post.firstname, post.lastname, post.id]);
        response.writeHead(302, {'Location': '/'});
        response.end();
    });
}

/**
 * @param request
 * @param response
 */
function remove(request, response) {

    params = url.parse(request.url, true).query;
    client.query("DELETE FROM users  WHERE id = $1",[params.id]);
    response.writeHead(302, {'Location': '/'});
    response.end();
}

exports.users = users;
exports.create = create;
exports.remove = remove;
exports.update = update;
exports.edit = edit;