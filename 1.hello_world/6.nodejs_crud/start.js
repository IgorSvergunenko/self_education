var http = require("http");
var url = require("url");

function start(handle) {

    function onRequest(request, response) {
        var pathname = url.parse(request.url).pathname;

        if (typeof handle[pathname] === 'function') {
            return handle[pathname](request, response);
        } else {
            response.writeHead(200, {"Content-Type": "text/plaine"});
            response.write("404 Not found");
            response.end();
        }

    }

    http.createServer(onRequest).listen(8888);
    console.log("Server has started.");
}

exports.start = start;