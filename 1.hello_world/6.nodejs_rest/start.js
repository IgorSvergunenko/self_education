var http = require("http");
var url = require("url");

function start(handle) {

    function onRequest(request, response) {

        var pathname = url.parse(request.url).pathname,
            action = pathname.substring(1, 6),
            userId = pathname.substring(7),
            isValidUserId = validateUserId(userId);

        if (action == 'users' && isValidUserId) {

            switch(request.method) {
                case 'GET':
                    return handle['users'](response, userId);
                    break;
                case 'POST':
                    return handle['create'](request, response);
                    break;
                case 'PUT':
                    return handle['update'](request, response, userId);
                    break;
                case 'DELETE':
                    return handle["remove"](response, userId);
                    break;
            }
        } else {
            response.writeHead(200, {"Content-Type": "text/plaine"});
            response.write("404 Not found");
            response.end();
        }

    }

    http.createServer(onRequest).listen(8888);
    console.log("Server has started.");
}

/**
 * Check is user id is number
 * @param userId
 * @returns {boolean}
 */
function validateUserId(userId) {

    if (userId == '') {
        return true;
    }

    var reg = new RegExp('^\\d+$');
    var isNumber = reg.test(userId);

    return isNumber;
}

exports.start = start;