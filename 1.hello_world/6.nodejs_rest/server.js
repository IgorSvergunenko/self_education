var server = require("./start");
var requestHandlers = require("./requestHandlers");

var handle = {}
handle["users"] = requestHandlers.users;
handle["create"] = requestHandlers.create;
handle["remove"] = requestHandlers.remove;
handle["update"] = requestHandlers.update;

server.start(handle);