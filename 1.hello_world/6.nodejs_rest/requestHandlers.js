var ejs = require('ejs'),
    fs = require("fs"),
    url = require("url"),
    pg = require("pg"),
    qs = require('querystring'),
    conString = "pg://postgres:postgres@localhost:5432/nodejs_crud";

// connect to db
var client = new pg.Client(conString);
client.connect();

//client.query("CREATE TABLE IF NOT EXISTS users(id SERIAL, firstname varchar(64), lastname varchar(64))");
//client.query("INSERT INTO users(firstname, lastname) values($1, $2)", ['Chuck', 'Berry']);
//client.query("INSERT INTO users(firstname, lastname) values($1, $2)", ['BB', 'King']);
//client.query("INSERT INTO users(firstname, lastname) values($1, $2)", ['Buddy', 'Guy']);
//client.query("INSERT INTO users(firstname, lastname) values($1, $2)", ['Eric', 'Clapton']);

/**
 * @param response
 * @param userId
 */
function users(response, userId) {

    if (userId !== "") {
        var query = client.query("SELECT id, firstname, lastname FROM users WHERE id = $1", [userId]);
    } else {
        var query = client.query("SELECT id, firstname, lastname FROM users");
    }

    query.on("row", function (row, result) {
        result.addRow(row);
    });

    query.on("end", function (result) {
        response.writeHead(200, {"Content-Type": "text/json"});
        response.write(JSON.stringify(result.rows, null, ""));
        response.end();
    });
}

/**
 * @param request
 * @param response
 */
function create(request, response) {

    var body = '';
    request.on('data', function (data) {
        body += data;
    });

    request.on('end', function () {

        var post = qs.parse(body);
        client.query("INSERT INTO users(firstname, lastname) values($1, $2)", [post.firstname, post.lastname]);
        response.writeHead(200, {"Content-Type": "text/json"});
        response.write('success');
        response.end();
    });
}

/**
 * @param request
 * @param response
 * @param userId
 */
function update(request, response, userId) {

    var params = url.parse(request.url, true).query;
    client.query("UPDATE users set firstname = $1, lastname = $2 WHERE id = $3", [params.firstname, params.lastname, userId]);
    response.writeHead(200, {"Content-Type": "text/json"});
    response.write('success');
    response.end();
}

/**
 * @param response
 * @param userId
 */
function remove(response, userId) {

    client.query("DELETE FROM users  WHERE id = $1",[userId]);
    response.writeHead(200, {"Content-Type": "text/json"});
    response.write('success');
    response.end();
}

exports.users = users;
exports.create = create;
exports.remove = remove;
exports.update = update;