//https://docs.nodejitsu.com/articles/HTTP/servers/how-to-read-POST-data

var http = require("http"),
    fs = require("fs");

var start = function(request, response) {

    var html = fs.readFileSync('index.html', "utf8"),
        params = "";

    //listen for incoming data
    request.on('data', function (data) {
        params = data;
    });

    //wait for data to finish
    request.on('end', function () {
        var username = params.toString().split('=').pop();

        if (username != '') {
            html = 'Hello, ' + username;
        }
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write(html);
        response.end();
    });
}

http.createServer(start).listen(8888);
