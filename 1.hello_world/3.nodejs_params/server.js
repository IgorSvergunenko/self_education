var http = require("http"),
    url = require("url"),
    fs = require("fs");

/**
 * Start server
 * @param request
 * @param response
 */
var start = function(request, response) {

    var query = parseUrl(request),
        html = "",
        status = 200,
        contentType =  "text/html";

    if (query.pathname !== "favicon.ico") {

        var fileName = query.pathname + '.html';

        // check is file exists
        if (fs.existsSync(fileName)) {
            html = fs.readFileSync(fileName, "utf8");
            html = html.replace('%username%', query.username);
        } else {
            status = 404;
            contentType = "text/plain";
            html = "404 Not Found";
        }
    }

    response.writeHead(status, {"Content-Type": contentType});
    response.write(html);
    response.end();

}

/**
 * Get path and parameter from url
 *
 * @param request
 * @returns {{pathname: (*|parseUrl.pathname|string), username: *}}
 */
var parseUrl = function(request) {

    var pathname = url.parse(request.url).pathname,
        params = url.parse(request.url, true).query,
        username = params.name ? params.name : "username";

    pathname = pathname.substring(1, pathname.length);
    pathname = pathname == "" ? "hello" : pathname;

    return {'pathname': pathname, 'username': username}
}

http.createServer(start).listen(8888);
