var http = require("http"),
    url = require("url"),
    fs = require("fs");

/**
 * Start server
 * @param request
 * @param response
 */
var start = function(request, response) {

    var pathname = url.parse(request.url).pathname;
    var contentType = 'text/html',
        mode = '',
        encoding = "utf8",
        extension = pathname.split('.').pop(),
        file = "." + pathname;

    if (pathname == "/"){
        file = "index.html";
    }

    switch(extension){
        case "png":
            contentType = 'image/png';
            mode = 'binary';
            encoding = '';
            break;
        case "css":
            contentType = 'text/css';
            break;
        case "html":
            contentType = 'text/html';
            break;
    }

    var fileToLoad = fs.readFileSync(file, encoding);
    response.writeHead(200, {'Content-Type':  contentType });
    response.write(fileToLoad, mode);
    response.end();
}

http.createServer(start).listen(8888);
