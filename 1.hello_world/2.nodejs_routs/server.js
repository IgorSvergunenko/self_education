var http = require("http"),
    url = require("url"),
    fs = require("fs");

/**
 * Start server
 * @param request
 * @param response
 */
var start = function(request, response) {

    var pathname = url.parse(request.url).pathname;
    pathname = pathname.substring(1, pathname.length);// get path without slash

    var html = "",
        status = 200,
        contentType =  "text/html";

    if (pathname !== "favicon.ico") {

        pathname = pathname == "" ? "hello" : pathname;
        var fileName = pathname + '.html';

        // check is file exists
        if (fs.existsSync(fileName)) {
            html = fs.readFileSync(fileName);
        } else {
            status = 404;
            contentType = "text/plain";
            html = "404 Not Found";
        }
    }

    response.writeHead(status, {"Content-Type": contentType});
    response.write(html);
    response.end();
}

http.createServer(start).listen(8888);
